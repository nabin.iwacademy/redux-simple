import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { switchEnable, switchDesable } from "../redux/action/switchAction";

const Switch = () => {
  const switches = useSelector((state) => state.switch);
  const dispatch = useDispatch();

  return (
    <div>
      <h1>Counter:{switches.enabled ? "enabled" : "disabled"}</h1>
      <button onClick={() => dispatch(switchEnable())}>Enabled</button>
      <button onClick={() => dispatch(switchDesable())}>Disabled</button>
    </div>
  );
};

export default Switch;

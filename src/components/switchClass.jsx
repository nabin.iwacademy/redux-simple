import React, { Component } from "react";
import { switchEnable, switchDesable } from "../redux/action/switchAction";
import { connect } from "react-redux";

export class SwitchClass extends Component {
  render() {
    return (
      <div>
        <h1>Counter:{this.props.switch.enabled ? "enabled" : "disabled"}</h1>
        <button onClick={() => this.props.switchEnable()}>Enabled</button>
        <button onClick={() => this.props.switchDesable()}>Disabled</button>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return { switch: state.switch };
};

export default connect(mapStateToProps, { switchEnable, switchDesable })(
  SwitchClass
);

import React from "react";
import Counter from "./components/Counter";
import Switch from "./components/Switch";
import SwitchClass from "./components/switchClass";
function App() {
  return (
    <div className="App">
      <Counter />
      <br />
      <Switch />
      <br />
      <SwitchClass />
    </div>
  );
}

export default App;

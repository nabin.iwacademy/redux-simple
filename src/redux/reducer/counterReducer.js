import {
    counterType
} from "../action/counterAction";
const initialValue = 0;
const counterReducer = (state = initialValue, action) => {
    switch (action.type) {
        case counterType.INCREMENT:
            return state + 1;
        case counterType.DECREMENT:
            return state - 1;

        case counterType.INCREMENTBYN:
            return state + action.payload;
        default:
            return state;
    }
};

export default counterReducer;
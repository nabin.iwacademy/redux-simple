import counterReducer from "./counterReducer";
import switchReducer from "./switchReducer";
import {
    combineReducers
} from "redux";

const allReducers = combineReducers({
    counter: counterReducer,
    switch: switchReducer,
});

export default allReducers;
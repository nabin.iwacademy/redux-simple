import {
    switchTypes
} from "../action/switchAction";
const initialValue = {
    enabled: false
};
const switchReducer = (state = initialValue, action) => {
    switch (action.type) {
        case switchTypes.SWITCH_ENABLE:
            return {
                enabled: true
            };
        case switchTypes.SWITCH_DESABLE:
            return {
                enabled: false
            };
        default:
            return {
                ...state
            };
    }
};

export default switchReducer;
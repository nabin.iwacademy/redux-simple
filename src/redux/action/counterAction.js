export const counterType = {
    INCREMENT: "INCREMENT",
    DECREMENT: "DECREMENT",
    INCREMENTBYN: "INCREMENTBYN"
};

export const increment = () => {
    return {
        type: counterType.INCREMENT
    };
};

export const decrement = () => {
    return {
        type: counterType.DECREMENT
    };

};

export const incrementByN = (payload) => {
    return {
        type: counterType.INCREMENTBYN,
        payload: payload
    };
};
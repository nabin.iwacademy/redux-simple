export const switchTypes = {
    SWITCH_ENABLE: "SWITCH_ENABLE",
    SWITCH_DESABLE: "SWITCH_DESABLE"
};

export const switchEnable = () => {
    return {
        type: switchTypes.SWITCH_ENABLE
    };
};

export const switchDesable = () => {
    return {
        type: switchTypes.SWITCH_DESABLE
    };
};